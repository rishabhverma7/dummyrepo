package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Product {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","/home/moglix/Downloads/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://buyerintuatui.moglilabs.com/");  
		
		driver.findElement(By.id("mat-input-0")).sendKeys("rahul@moglix.com");
		driver.findElement(By.id("inputPassword3")).sendKeys("dontKnow");
		driver.findElement(By.className("btn")).click();
        Thread.sleep(5000);
		
		driver.findElement(By.cssSelector("body > app-root > app-layout > app-main-layout > div > app-sidebar > div > div > ul > li:nth-child(6) > div > i")).click();
		
		driver.findElement(By.xpath("//button[contains(text(),'CREATE PRODUCT')]")).click();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		
		driver.findElement(By.id("mat-select-9")).click();
	    driver.findElement(By.id("mat-input-19")).sendKeys("Buyer Testing");
	    driver.findElement(By.id("mat-option-1289")).click();
	            
	    driver.findElement(By.id("mat-select-10")).click();
	    driver.findElement(By.id("mat-input-21")).sendKeys("8855");
	    driver.findElement(By.id("mat-option-1735")).click();
		
		
		WebElement name = driver.findElement(By.id("mat-input-13"));
        name.click();
        name.sendKeys("Product_02");
        
        WebElement name1 = driver.findElement(By.id("mat-input-14"));
        name1.click();
        name1.sendKeys("ProductCode_01");
        
        driver.findElement(By.id("mat-select-11")).click();
        driver.findElement(By.id("mat-option-1584")).click();
        
        WebElement name2 = driver.findElement(By.id("mat-input-15"));
        name2.click();
        name2.sendKeys("1000");
        
        WebElement name3 = driver.findElement(By.id("mat-input-16"));
        name3.click();
        name3.sendKeys("25000");
        
        driver.findElement(By.id("mat-select-12")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        clickOn(driver, driver.findElement(By.id("mat-option-1621")),20);
        
        driver.findElement(By.id("mat-input-17")).click();
        driver.findElement(By.id("mat-option-1628")).click();
        
        driver.findElement(By.xpath("//button[contains(text(),'CREATE PRODUCT')]")).click();
        Thread.sleep(2000);
        
        driver.findElement(By.id("mat-select-13")).click();
        driver.findElement(By.id("mat-input-31")).sendKeys("Buyer Testing");
        driver.findElement(By.id("mat-option-2565")).click();
        
        driver.findElement(By.id("mat-select-14")).click();
        driver.findElement(By.id("mat-input-33")).sendKeys("8855");
        driver.findElement(By.id("mat-option-2857")).click();
        driver.findElement(By.xpath("//button[contains(text(),'APPLY')]")).click();
        
        Thread.sleep(4000);
        driver.quit();
	
        
	}
	public static void clickOn(WebDriver driver, WebElement locator, int timeout) {
		new WebDriverWait(driver, timeout).ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(locator));
		locator.click();
	}

}
