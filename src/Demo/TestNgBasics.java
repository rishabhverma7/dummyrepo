package Demo;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNgBasics {

	WebDriver driver;
	@BeforeTest
	public void setUpTest() {
		
		System.setProperty("webdriver.chrome.driver","/home/moglix/Downloads/chromedriver_linux64/chromedriver");
	    driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://eoc-qa1.moglilabs.com/");  
	}
	
	 
	
	@Test
	public void login() throws InterruptedException {
		System.out.println("EOC");
		driver.findElement(By.xpath("//div[@id=\"defaultForm\"]")).click();
		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[contains(text(),'CREATE PO')]")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("mat-select-6")).click();
        driver.findElement(By.xpath("//span[@class='mat-option-text' and contains(text(),' (8653) Best Buyer ')]")).click();
	    
        driver.findElement(By.id("mat-select-7")).click();
        driver.findElement(By.xpath("//span[@class='mat-option-text' and contains(text(),' (8855) Buyer Testing ')]")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("mat-select-8")).click();
        driver.findElement(By.xpath("//span[@class='mat-option-text' and contains(text(),' Mogli Labs (India) Private Limited - Delhi ')]")).click();
        Thread.sleep(2000);
        WebElement ponumber = driver.findElement(By.id("mat-input-10"));
        ponumber.click();
        ponumber.sendKeys("PO1234");
        
        driver.findElement(By.id("mat-input-13")).sendKeys("7/25/2020");
        driver.findElement(By.id("mat-input-14")).sendKeys("7/25/2020");
        WebElement poc = driver.findElement(By.id("mat-input-15"));
        poc.click();
        poc.sendKeys("Rishabh-9876543212");
       
        
        driver.findElement(By.xpath("//input[@class='ng-untouched ng-pristine ng-invalid']")).sendKeys("/home/moglix/Downloads/Sample-Test-Case-Template-1.png");
        Thread.sleep(5000);
       //customized xpath
        driver.findElement(By.xpath("//button[contains(@class,'btn save-btn ng-star-inserted')]")).click();
        driver.findElement(By.id("file_id")).sendKeys("/home/moglix/Downloads/item-template.xlsx");
        Thread.sleep(3000);
        driver.findElement(By.xpath("//button[contains(@class,'btn save-btn ng-star-inserted')]")).click();
        
        driver.findElement(By.xpath("//body/app-root[1]/app-layout[1]/app-main-layout[1]/div[1]/div[1]/div[1]/app-punch[1]/app-executive-summary[1]/div[1]/form[1]/div[4]/mat-checkbox[1]/label[1]/div[1]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
           Thread.sleep(4000); 
           
            
           driver.findElement(By.xpath("/html/body/app-root/app-layout/app-main-layout/div/app-sidebar/div/div/div[1]/i']")).click();
           
           driver.findElement(By.xpath("//i[contains(@class,'material-icons togglesidebar']")).click();
		
		
	}



}


	
	

