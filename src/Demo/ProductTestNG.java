package Demo;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class ProductTestNG {
	
WebDriver driver;
	
	@BeforeMethod
	public void OpenBrowser() {
		System.setProperty("webdriver.chrome.driver","/home/moglix/Downloads/chromedriver_linux64 (1)/chromedriver");
	    driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://buyerintuatui.moglilabs.com/");  
		System.out.println("Open the Browser");
	}
	
	@Test
	public void tc1() {
		driver.findElement(By.id("mat-input-0")).sendKeys("rahul@moglix.com");
		driver.findElement(By.id("inputPassword3")).sendKeys("dontKnow");
		driver.findElement(By.className("btn")).click();
		System.out.println("TestCase1");
	  
	}
	
	
	@AfterMethod
	public void CloseBrowser() {
		System.out.println("Close the Browser");
	}

}
