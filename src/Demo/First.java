package Demo;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class First {

	public static void main(String[] args) throws InterruptedException, AWTException {
		// TODO Auto-generated method stub
     
		System.setProperty("webdriver.chrome.driver","/home/moglix/Downloads/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://buyerintuatui.moglilabs.com/"); 
		Thread.sleep(5000);
		driver.findElement(By.xpath("//div[@id=\"defaultForm\"]")).click();
	
		//driver.findElement(By.id("mat-input-0")).sendKeys("rahul@moglix.com");
		//driver.findElement(By.id("inputPassword3")).sendKeys("dontKnow");
		driver.findElement(By.className("btn")).click();
        Thread.sleep(6000);
		//click on create po button
        driver.findElement(By.xpath("//button[contains(text(),'CREATE PO')]")).click();
        //driver.findElement(By.xpath("//button[contains(@type,'button')]")).click();
        Thread.sleep(1000);
        
        
        //dropdown
        driver.findElement(By.id("mat-select-6")).click();
        driver.findElement(By.id("mat-input-20")).sendKeys("Buyer Testing");
        driver.findElement(By.id("mat-option-798")).click();
        
        //for dynamic id
        //driver.findElement(By.xpath("//input[starts-with(@id,'mat-option-')]")).click();
        
        driver.findElement(By.id("mat-select-7")).click();
        driver.findElement(By.id("mat-input-22")).sendKeys("8855");
        driver.findElement(By.id("mat-option-1139")).click();
       
        Thread.sleep(1000);
        driver.findElement(By.id("mat-select-8")).click();
        driver.findElement(By.id("mat-option-1128")).sendKeys("Mogli Labs (India) Private Limited - Delhi");
        driver.findElement(By.id("mat-option-1128")).click();
        
        WebElement ponumber = driver.findElement(By.id("mat-input-10"));
        ponumber.click();
        ponumber.sendKeys("PO1234");
        driver.findElement(By.id("mat-input-13")).sendKeys("7/25/2020");
        driver.findElement(By.id("mat-input-14")).sendKeys("7/25/2020");
       
        driver.findElement(By.xpath("//input[@class='ng-untouched ng-pristine ng-invalid']")).sendKeys("/home/moglix/Downloads/seleniumusefile.png");
        Thread.sleep(5000);
       //customized xpath
        driver.findElement(By.xpath("//button[contains(@class,'btn save-btn ng-star-inserted')]")).click();
        
       //upload file
        driver.findElement(By.id("file_id")).sendKeys("/home/moglix/Downloads/sample-item-template-v2.xlsx");
        Thread.sleep(3000);
        driver.findElement(By.xpath("//button[contains(@class,'btn save-btn ng-star-inserted')]")).click();
        
        
        Thread.sleep(2000);
        driver.findElement(By.xpath("//body/app-root[1]/app-layout[1]/app-main-layout[1]/div[1]/div[1]/div[1]/app-punch[1]/app-executive-summary[1]/div[1]/form[1]/div[4]/mat-checkbox[1]/label[1]/div[1]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
        
        
        //driver.findElement(By.id("//i[contains(text(),'layers')]")).click();
        driver.findElement(By.xpath("//body/app-root[1]/app-layout[1]/app-main-layout[1]/div[1]/app-sidebar[1]/div[1]/div[1]/ul[1]/li[2]/div[1]")).click();
        
        Thread.sleep(8000);
		driver.quit();
	   	
		
	}	
	}
