package Demo;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TakeScreenshot {

	WebDriver driver;
	@Test
	public void Open() throws Exception {
		
		System.setProperty("webdriver.chrome.driver","/home/moglix/Downloads/chromedriver_linux64 (1)/chromedriver");
		WebDriver driver = new ChromeDriver();
	     driver.get("https://www.google.co.in");
	     try {
	     driver.findElement(By.id("sdsdsd")).sendKeys("test");
	     }
	     catch(Exception e) {
	    	 System.out.println("There is an Exception");
	     }
	     getScreenshot();
	     
	}
	
	
	public void getScreenshot() throws Exception {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("/home/moglix/Pictures/screenshot.png"));
		
	}
}
